/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.Client;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulino Zelaya
 */
@Entity
@Table(name = "Client", catalog = "Optica_Calero", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findByIDClient", query = "SELECT c FROM Client c WHERE c.iDClient = :iDClient"),
    @NamedQuery(name = "Client.findByAge", query = "SELECT c FROM Client c WHERE c.age = :age"),
    @NamedQuery(name = "Client.findByCed", query = "SELECT c FROM Client c WHERE c.ced = :ced"),
    @NamedQuery(name = "Client.findByIDPerson", query = "SELECT c FROM Client c WHERE c.iDPerson = :iDPerson")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_Client")
    private Integer iDClient;
    @Basic(optional = false)
    @Column(name = "Age")
    private Integer age;
    @Basic(optional = false)
    @Column(name = "Ced")
    private String ced;
    @Column(name = "ID_Person")
    private Integer iDPerson;

    public Client() {
    }

    public Client(Integer iDClient) {
        this.iDClient = iDClient;
    }

    public Client(Integer iDClient, Integer age, String ced) {
        this.iDClient = iDClient;
        this.age = age;
        this.ced = ced;
    }

    public Integer getIDClient() {
        return iDClient;
    }

    public void setIDClient(Integer iDClient) {
        this.iDClient = iDClient;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCed() {
        return ced;
    }

    public void setCed(String ced) {
        this.ced = ced;
    }

    public Integer getIDPerson() {
        return iDPerson;
    }

    public void setIDPerson(Integer iDPerson) {
        this.iDPerson = iDPerson;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDClient != null ? iDClient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.iDClient == null && other.iDClient != null) || (this.iDClient != null && !this.iDClient.equals(other.iDClient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Client.Client[ iDClient=" + iDClient + " ]";
    }
    
}
